#!/usr/bin/env bash
# Example Shell Script File to Bootstrap the Application in Vagrant
# Prevent
export DEBIAN_FRONTEND=noninteractive

# Update Ubuntu
printf "Step 1 of 3: Updating Ubuntu Repository..."
apt-get update -y > /dev/null

#Download and Install Microsoft dotnet Repositorys and Keys
printf "Step 2 of 3: Download and Install Microsoft dotnet Repositorys and Keys..."
wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

# Install dotnet with APT Package Manager
printf "Step 3 of 3: Install dotnet with Package Manager."
sudo apt-get install -y apt-transport-https
sudo apt-get update
sudo apt-get install -y dotnet-sdk-2.1

# Status Complete
printf "%s\nCOMPLETE: SAM vlab Provisioning COMPLETE!!"
printf "%s\nTo login to the Vagrant VM use vagrant ssh in the current directory"
printf "%s\nThe URL http://localhost:8082 is exposed to the Host"