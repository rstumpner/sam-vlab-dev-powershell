# Update Package Source
exec { "apt-update":
    command => "/usr/bin/apt-get update"
}

exec { "apt-upgrade":
    command => "/usr/bin/apt-get -y upgrade"
}

#file { "/etc/apt/sources.list.d/foreman.list":
#  ensure => present,
#  content => "deb http://deb.theforeman.org/ trusty 1.9 \n deb http://deb.theforeman.org/ plugins 1.9",
#}

#exec { "update keys":
#    command => "wget -q http://deb.theforeman.org/pubkey.gpg -O- | apt-key add -",
#    require => Exec['apt-update'],
#}
package { "links":
   ensure => latest;
}

package { "apache2":
  ensure  => latest,
  require  => Exec['apt-update','apt-upgrade'],
}


